package thebookstore;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.SynchronousQueue;


public class thebookstore {
	static ArrayList<Books> booksList = new ArrayList<Books>();
	public static void main(String[] args) {
		init();//初始化书架
		//将书架上所有图书信息打印出来
		for(int i = 0; i<booksList.size();i++) {
			System.out.println(booksList.get(i));
		}
		while(true){
			//获取控制台输入信息
			Scanner scan = new Scanner(System.in);
			System.out.println("请输入图书编号：");
			int bookId = scan.nextInt();
			Books stockBooks = getBooksById(bookId);
			if(stockBooks != null) {//判断是否存在该图书
				System.out.println("当前图书信息" + stockBooks);
				System.out.println("请输入购买数量：");
				int bookNumber = scan.nextInt();
				if(bookNumber <= stockBooks.number) {//判断库存是否足够
					//将输入信息封装成Books对象
					Books books = new 	Books(stockBooks.id, stockBooks.name,
							stockBooks.price, bookNumber,
							stockBooks.price * bookNumber, stockBooks.publish);
					Testbookstore.saveBooks(books);
					//修改库存
					stockBooks.setNumber(stockBooks.number - bookNumber);
				}else {
					System.out.println("库存不足！");
				}
			}else {
				System.out.println("图书编号输入错误！");
			}
		}
	}
	/**
	 * 初始化书架上图书的信息，将图书放到书架上
	 */
	private static void init() {
		Books goods1 = new Books(1, "四级词汇",29.00,100,80000,"CET4出版社");
		Books goods2 = new Books(2, "六级词汇",41.00,150,45000,"CET6出版社");
		Books goods3 = new Books(3, "雅思词汇",55.00,100,70000,"IELTS出版社");
		Books goods4 = new Books(4, "托福词汇",51.00,210,5000,"TOEFL出版社");
		booksList.add(goods1);
		booksList.add(goods2);
		booksList.add(goods3);
		booksList.add(goods4);
	}
	/**
	 * 根据输入的图书编号查找图书信息 循环遍历书架中图书信息，找到图书编号相等的取出
	 */
	private static Books getBooksById(int bookId) {
		for (int i = 0; i <booksList.size(); i++) {
			Books thisBooks = booksList.get(i);
			if(bookId == thisBooks.id) {
				return thisBooks;
			}
		}
		return null;
	}
}



