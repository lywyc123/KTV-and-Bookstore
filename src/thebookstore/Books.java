package thebookstore;

public class Books {
	int id;
	//图书名称
	String name;
	//图书单价
	double price;
	//图书数量
	int number;
	//总价
	double money;
	//出版社
	String publish;
	public Books(int id, String name, double price,
			int number, double money, String publish){
		this.id = id;
		this.name = name;
		this.price = price;
		this.number = number;
		this.money = money;
		this.publish = publish;
	}
	public String toStringt() {
		String message = "图书编号：" + id + "图书名称：" +name+"出版社" + publish+ "单价" + price+ "库存数量：" +number;
		return message;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	
}


