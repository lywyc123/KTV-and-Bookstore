package com.hnevc.javawork;
/*
 * 9、编写程序，编写数组，保存一些人的名字，并统计姓名所有姓张的人的数目。
 */
public class Test9 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int count =0; 
		String[] names = new String[]{"王玉成","诗卿","轻衣","全芬林","黄雨琪"};
		
		for(int i=0;i<names.length;i++){
		    if(	names[i].charAt(0) == '王' ){
		    	count ++;
		    	System.out.println(names[i]);
		    }
		}
		System.out.println(count);

	}

}
