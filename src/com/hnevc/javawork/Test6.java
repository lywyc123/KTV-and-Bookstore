package com.hnevc.javawork;

import java.util.Random;

/*
 * 使用数组，保存10个随机数（使用Random类生成）
 */

public class Test6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// 声明一个10个元素的整形数组
		int[] nums = new int[10];
		// nums[0] nums[1] …… nums[9]          XXXX nums[10]不存在
		
		for(int i=0;i<nums.length;i++){
			Random random = new Random(); // 创建一个random对象
			int num = random.nextInt(46)+55;
			nums[i] = num;
		}
		// 数组元素的遍历输出
		for(int j=0;j<nums.length;j++){
			System.out.println(nums[j]);
		}
		
		
		
		
		
		
		
	}

}

