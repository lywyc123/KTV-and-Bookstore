package com.hnevc.javawork;

import java.util.Scanner;

/*
 * 8、编写程序，输入一个人的姓名，提取这个人名字的第一个字。
 */
public class Test8 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);// 定义输入对象scanner
		String name = scanner.next();//接受字符串
		char a = name.charAt(0);// 提前第一个字符
		System.out.println(a);// 打印输出字符a
	}

}
