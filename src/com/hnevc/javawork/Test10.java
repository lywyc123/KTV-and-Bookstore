package com.hnevc.javawork;

import java.util.Random;

/*
 * 使用数组，保存10个随机整数，选出其中的最大值
 */
public class Test10 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] nums = new int[10];
		
		Random random = new Random();
		for(int i=0;i<nums.length;i++){
			nums[i] = random.nextInt(100);
		}
		
		int max=0;// 目前的最大值
		for(int k=0;k<nums.length;k++){
			if(nums[k] > max){
				max = nums[k];
			}
		}
		System.out.println(max);
		
	}

}
