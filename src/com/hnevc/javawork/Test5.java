package com.hnevc.javawork;

import java.util.Random;

/*
 * 5、编程程序，使用Random类，生成一个1~100（包含1，和100）的随机整数。
 */
public class Test5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Random random = new Random();
		int num = random.nextInt(46)+55;
		System.out.println(num);
	}

}
