package com.hnevc.javawork;

import java.util.Random;

/*
 * 使用数组，保存10个随机的整数（使用Random类生成），然后对其进行升序排序。
 */
public class Test7 {
	public static void main(String[] args){
		int[] nums = new int[10];// 定义数组
		for(int i=0;i<nums.length; i++){// 生成10个随机数
			Random random = new Random();
			nums[i] = random.nextInt(100);
		}
		for(int a =0;a < nums.length - 1 ; a++){	// k 轮数
			for(int b=0;b< nums.length -1; b++){// m 表示当前比较的两个元素的 小 下标
				// 比较 nums[b],nums[b+1]
				if(nums[b] > nums[b+1])
				{// 交换nums[b],nums[b+1]
					int temp =0; // temp 交换用临时变量
					temp = nums[b+1];
					nums[b+1] = nums[b];
					nums[b] = temp;
				}
			}
		}	
										
		for(int j=0;j<nums.length ; j++){// 输出
			System.out.println(nums[j]);
		}
		
		
		
	}
}
