package com.hnevc.javawork;

import org.omg.CORBA.SystemException;

/*
 * 2、在main方法中，定义两个整形变量，赋值为3和4，然后交换两个变量的值。
 */
public class Test2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a;
		int b;
		a = 3;
		b = 4;

		int temp = 0;
		
		temp = b;
		b = a;
		a = temp;
		
		System.out.println("a" + a + "===b" + b);

	}

}