package com.hnevc.javawork;

import java.util.Scanner;

/*
 * 4、在main方法中，编写程序，根据邮寄自费规则：“长沙寄送到上海快递，
 * 首重12元，续重10元每千克”，编写程序自动计算快递费。
 */
public class Test4 {
	
	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);
		double weight = scanner.nextDouble(); 
		int fee=0; 
		if(weight <=1.0){
			fee = 12;
		}else{
			
			fee = 12 + (int)(Math.ceil(weight - 1 )*10);
		}
		System.out.println(fee+"元");
	}
}
