package theKTV;

import java.util.LinkedList;
import java.util.Scanner;

public class Singing {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("------------欢迎来到点歌系统-----------");
		System.out.println("1.添加歌曲至系统");
		System.out.println("2.将歌曲置顶");
		System.out.println("3.将歌曲前移一位");
		System.out.println("4.退出");
		LinkedList lineUpList = new LinkedList();//创建歌曲列表
		addMusicList(lineUpList);
		while(true) {
			System.out.println("请输入要执行的操作序号");
			Scanner scan = new Scanner(System.in);
			int command = scan.nextInt();
			switch(command) {
			case 0:
				addMusic(lineUpList);
				break;
			case 1:
				setTop(lineUpList);
				break;
			case 2:
				setBefore(lineUpList);
				break;
			case 3:
				exit();
				break;
			default:
				System.out.println("------------------------------");
				System.out.println("你输入的有误，请重新输入序号！");
				break;
			}
			System.out.println("当前歌曲列表："+lineUpList);
		}
	}
	//初始时添加歌曲的名称
	private static void addMusicList(LinkedList lineUpList) {
		lineUpList.add("起风了");
		lineUpList.add("你并不懂我");
		lineUpList.add("Flashlight");
		lineUpList.add("痴心绝对");
		lineUpList.add("花田错");
		System.out.println("初始歌曲列表"+lineUpList);
	}
	//执行添加歌曲
	private static void addMusic(LinkedList lineUpList) {
		System.out.println("请输入要添加的歌曲名称：");
		String musicName = new Scanner(System.in).nextLine();
		lineUpList.addLast(musicName);
		System.out.println("已添加歌曲："+musicName);
	}
	//执行将歌曲置顶
	private static void setTop(LinkedList lineUpList) {
		System.out.println("请输入置顶的歌曲名称：");
		String musicName = new Scanner(System.in).nextLine();
		int position = lineUpList.indexOf(musicName);
		if(position < 0) {
			System.out.println("当前列表中没有输入的歌曲");
		}else {
			lineUpList.remove(musicName);
			lineUpList.addFirst(musicName);
		}
		System.out.println("已将歌曲" + musicName + "置顶");
	}
	//执行将歌曲置前一位
	private static void setBefore(LinkedList lineUpList) {
		System.out.println("请输入要置前的歌曲名称：");
		String musicName = new Scanner(System.in).nextLine();
		int position = lineUpList.indexOf(musicName);
		if(position < 0) {
			System.out.println("当前列表没有输入歌曲");
		}else if (position == 0) {
			System.out.println("当前歌曲已置顶");
		}else {
			lineUpList.remove(musicName);
			lineUpList.add(position - 1, musicName);
		}
		System.out.println("已将歌曲" + musicName + "置前一位");
	}
	//退出
	private static void exit() {
		System.out.println("--------------------");
		System.out.println("您已退出系统");
		System.out.println();
	}
}




